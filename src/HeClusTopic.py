import numpy as np
import torch
import torch.nn as nn
import torch.nn.functional as F
from transformers import BertModel, BertTokenizer
import utils
from utils import GMMUtils



class NVDM_GSM(nn.Module):
    '''
    正态分布假设。
    '''
    def __init__(self, config):
        super(NVDM_GSM, self).__init__()
        encode_dims = [config.vocab_size, 1024, 512, config.n_topics]
        hidden_dim = config.hidden_dim

        # encoder
        self.encoder = nn.ModuleList([
            nn.Linear(encode_dims[i], encode_dims[i+1]) for i in range(len(encode_dims)-2)
        ])

        # 期望和对数方差
        self.fc_mu = nn.Linear(encode_dims[-2], encode_dims[-1])
        self.fc_logvar = nn.Linear(encode_dims[-2], encode_dims[-1])
        nn.init.constant_(self.fc_logvar.weight, 0.0)     
        nn.init.constant_(self.fc_logvar.bias, 0.0)     

        # gsm
        self.fc_gsm = nn.Linear(config.n_topics, config.n_topics)

        # decoder矩阵：beta = phi x psi
        self.phi = nn.Linear(hidden_dim, config.n_topics)
        self.psi = nn.Linear(hidden_dim, config.vocab_size)

    def reparameterize(self, mu, logvar):
        '''
        重参方法，基于encoder给出的期望和对数方差生成z。
        '''
        eps = torch.randn_like(mu) # 返回与mu大小一致且服从0-1正态分布的tensor
        std = torch.exp(logvar/2)
        z = mu + eps*std
        return z        

    def encode(self, x):
        hid = x
        for layer in self.encoder:
            hid = F.relu(layer(hid))
        mu, logvar = self.fc_mu(hid), self.fc_logvar(hid)
        return mu, logvar

    def decode(self, theta):
        # decode得到的已经是0-1的值，之后无需再进行softmax
        weight = self.phi(self.psi.weight) # (vocab_size, num_topic)
        beta_weight = F.softmax(weight, dim=0).transpose(1, 0) # (num_topic, vocab_size)
        logits = torch.matmul(theta, beta_weight)
        # almost_zeros = torch.full_like(logits, 1e-6)
        # logits = logits.add(almost_zeros)
        return logits   

    def inference_theta(self, x):
        mu, logvar = self.encode(x)
        z = self.reparameterize(mu, logvar)
        theta = F.softmax(self.fc_gsm(z), dim=1) # gsm
        return theta             

    def forward(self, x):
        mu, logvar = self.encode(x)
        z = self.reparameterize(mu, logvar)
        theta = F.softmax(self.fc_gsm(z), dim=1) # gsm
        logits = self.decode(theta)
        # print(logits)
        return logits, mu, logvar, theta
    
    def loss(self, x, logits, mu, logvar):
        rec_loss = -1 * torch.sum(x * torch.log(logits))
        kld_loss = -0.5 * torch.sum(1 + 2*logvar - mu**2 - torch.exp(2 * logvar)) # 分布约束正则项
        loss = rec_loss + kld_loss
        loss_dict = {
            "rec_loss": rec_loss,
            "kld_loss": kld_loss,
            "loss": loss
        }
        return loss_dict

    def get_beta(self, return_tensor=False):
        weight = self.phi(self.psi.weight) # (vocab_size, num_topic)
        beta_weight = F.softmax(weight, dim=0).transpose(1, 0) # (num_topic, vocab_size)
        if not return_tensor:
            beta_weight = beta_weight.detach().cpu().numpy()
        return beta_weight



class AttnDocEncoder(nn.Module):
    def __init__(self, config) -> None:
        super().__init__()
        self.config = config
        self.bert = BertModel.from_pretrained("bert-base-uncased")
        for p in self.bert.parameters():
            p.requires_grad = False
        self.bert.eval()
        tokenizer = BertTokenizer.from_pretrained('bert-base-uncased', do_lower_case=True)
        self.vocab = tokenizer.get_vocab()        
        self.inv_vocab = {v:k for k, v in self.vocab.items()}        
        self.bert_dim = 768
        self.scale = self.bert_dim ** -0.5
        self.query = nn.Linear(self.bert_dim, self.bert_dim)
        self.key = nn.Linear(self.bert_dim, self.bert_dim)
        self.value = nn.Linear(self.bert_dim, self.bert_dim)
        self.ref_fc = nn.Linear(self.bert_dim, self.bert_dim)
        self.reduce_fc = nn.Linear(self.bert_dim, self.config.latent_dim)
        self.cls_fc = nn.Sequential(
            nn.Linear(self.config.latent_dim, 128),
            nn.ReLU(),
            nn.Linear(128, 64),
            nn.ReLU(),
            nn.Linear(64, self.config.n_clusters),
        )
    

    def get_cls_output(self, input_ids, attention_masks, valid_pos):
        doc_embs = self(input_ids, attention_masks, valid_pos)
        cls_output = F.softmax(self.cls_fc(doc_embs), dim=-1)
        return cls_output


    def forward(self, input_ids, attention_masks, valid_pos, return_weights=False):
        '''
        param: input_ids: shape=(batch_size, seq_length)
        param: attention_masks: shape=(batch_size, seq_length)
        param: valid_pos: shape=(batch_size, seq_length), 0/1
        '''
        batch_size, seq_length = input_ids.shape
        attn_mask = valid_pos == 0
        attn_mask_multi = torch.stack([attn_mask]*seq_length, dim=1)
        
        bert_out = self.bert(input_ids, attention_mask=attention_masks)[0] # shape=(batch_size, seq_length, bert_dim)
        query_embs, key_embs, value_embs = self.query(bert_out), self.key(bert_out), self.value(bert_out)
        
        attn_logits = torch.matmul(query_embs, key_embs.transpose(-1,-2)) / self.scale # shape=(batch_size, seq_length, seq_length)
        attn_logits.masked_fill_(attn_mask_multi, float('-inf'))
        attn_weights = F.softmax(attn_logits, dim=-1) # shape=(batch_size, seq_length, seq_length)
        attn_embs = torch.matmul(attn_weights, value_embs) # shape=(batch_size, seq_length, bert_dim)

        ref_emb = self.ref_fc(torch.sum(attn_embs * (valid_pos/valid_pos.sum(dim=1).unsqueeze(dim=1)).unsqueeze(dim=2), dim=1))
        doc_attn_logits = torch.matmul(ref_emb.unsqueeze(dim=1), attn_embs.transpose(-1,-2)).squeeze() # shape=(batch_size, bert_dim)
        doc_attn_logits.masked_fill_(attn_mask, float('-inf'))
        doc_attn_weights = F.softmax(doc_attn_logits, dim=-1)
        doc_emb = self.reduce_fc(torch.sum(doc_attn_weights.unsqueeze(dim=2) * bert_out, dim=1))
        if return_weights == True:
            return doc_emb, doc_attn_weights
        # check attn weights
        # test = list(sorted(zip(input_ids[0].detach().cpu().numpy(), doc_attn_weights[0].detach().cpu().numpy()), key=lambda p:p[1], reverse=True))[:10]
        # print([(p[1], self.inv_vocab[int(p[0])]) for p in test])            
        return doc_emb


class AutoEncoderNTM(nn.Module):
    def __init__(self, config) -> None:
        super().__init__()
        encode_dims = [config.vocab_size, 1024, 512, config.n_topics]
        hidden_dim = config.hidden_dim

        # encoder
        self.encoder = nn.ModuleList([
            nn.Linear(encode_dims[i], encode_dims[i+1]) for i in range(len(encode_dims)-1)
        ])
        # gsm
        self.fc_gsm = nn.Linear(config.n_topics, config.n_topics)

        # decoder矩阵：beta = phi x psi
        self.phi = nn.Linear(hidden_dim, config.n_topics)
        self.psi = nn.Linear(hidden_dim, config.vocab_size)    

    def encode(self, x):
        hid = x
        for layer in self.encoder[:-1]:
            hid = F.relu(layer(hid))
        hid = self.encoder[-1](hid)
        return hid

    def decode(self, theta):
        weight = self.phi(self.psi.weight) # (vocab_size, num_topic)
        beta_weight = F.softmax(weight, dim=0).transpose(1, 0) # (num_topic, vocab_size)
        preds = torch.matmul(theta, beta_weight)
        return preds   

    def inference_theta(self, x):
        mu, logvar = self.encode(x)
        z = self.reparameterize(mu, logvar)
        theta = F.softmax(self.fc_gsm(z), dim=1) # gsm
        return theta             

    def forward(self, x):
        z = self.encode(x)
        theta = F.softmax(self.fc_gsm(z), dim=1) # gsm
        preds = self.decode(theta)
        # print(preds)
        return preds
    
    def loss(self, x, preds):
        rec_loss = -1 * torch.sum(x * torch.log(preds))
        # print("rec loss: {:.5f}, kld loss: {:.5f}".format(rec_loss, kld_loss))
        return rec_loss

    def get_beta(self, return_tensor=False):
        weight = self.phi(self.psi.weight) # (vocab_size, num_topic)
        beta_weight = F.softmax(weight, dim=0).transpose(1, 0) # (num_topic, vocab_size)
        if not return_tensor:
            beta_weight = beta_weight.detach().cpu().numpy()
        return beta_weight




class HeClusTopic(nn.Module):
    def __init__(self, config):
        super(HeClusTopic, self).__init__()
        self.config = config
        self.device = config.device

        # nn.Parameter 参与参数优化
        self.gmm_pi = nn.Parameter(torch.zeros(config.n_clusters)) # gmm param
        self.gmm_mu = nn.Parameter(torch.randn(config.n_clusters, config.latent_dim)) # gmm param
        self.gmm_logvar = nn.Parameter(torch.randn(config.n_clusters, config.latent_dim)) # gmm param

        self.vae = NVDM_GSM(config)
        self.doc_encoder = AttnDocEncoder(config)
        self.dependency = nn.Linear(config.n_clusters, config.n_topics)

    
    def init_gmm(self, mu, logvar, pi):
        '''
        由GMM模型初始化参数
        '''
        self.gmm_mu.data = torch.tensor(mu).float().to(self.device)
        self.gmm_logvar.data = torch.tensor(logvar).float().to(self.device)
        self.gmm_pi.data = torch.tensor(pi).float().to(self.device)

    def init_dependency_mtx(self, init_mtx):
        self.dependency.weight = nn.Parameter(init_mtx.T)

    @property
    def gmm_weights(self):
        return torch.softmax(self.gmm_pi, dim=0)

    @property
    def dependency_weights(self):
        '''
        Return dependency matrix softmax weights, shape = (n_global_topics, n_local_topics)
        '''
        return F.softmax(self.dependency.weight, dim=0).T

    def inference_theta(self, x):
        theta = self.vae.inference_theta(x)
        return theta             

    def get_doc_embs(self, input_ids, attention_mask, valid_pos, return_weights=False):
        return self.doc_encoder(input_ids, attention_mask, valid_pos, return_weights)

    def forward(self, input_ids, attention_masks, valid_pos, bows):
        doc_embs = self.doc_encoder(input_ids, attention_masks, valid_pos)
        logits, mu, logvar, theta = self.vae(bows)
        return doc_embs, logits, mu, logvar, theta

    def loss(self, doc_embs, bows, logits, mu, logvar, theta):
        '''
        x: size=(batch_size, vocab_size)
        mu: size=(batch_size, n_topic_leaf)
        logvar: size=(batch_size, n_topic_leaf)
        vecs: size=(vocab_size, config.latent_dim)
        global_topic_weights: shape=(n_topic_global)
        '''
        dependency = self.dependency_weights

        rec_loss = -1 * torch.sum(bows * torch.log(logits))
        kld_loss = -0.5 * torch.sum(1 + 2*logvar - mu**2 - torch.exp(2 * logvar))
        target_theta = torch.mm(GMMUtils.predict_proba(doc_embs, self.gmm_mu, torch.exp(self.gmm_logvar), self.gmm_weights), dependency)
        theta_kld_loss = F.kl_div(theta.log(), target_theta, reduction="batchmean")

        loss = rec_loss + kld_loss + theta_kld_loss

        loss_dict = {
            "rec_loss": rec_loss,
            "kld_loss": kld_loss,
            "theta_kld_loss": theta_kld_loss,
            "loss": loss
        }

        return loss_dict

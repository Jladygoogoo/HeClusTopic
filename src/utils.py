import wandb
import pynvml
import pickle
from datetime import datetime
from collections import Counter
from tqdm import tqdm
import torch
from gensim.corpora.mmcorpus import MmCorpus
from nltk.corpus import stopwords
from transformers import BertTokenizer
import os
import string
from nltk.tag import pos_tag
from sklearn.cluster import KMeans
import numpy as np
from sklearn.metrics.cluster import normalized_mutual_info_score
from sklearn import metrics, manifold
import matplotlib.pyplot as plt
from sklearn.decomposition import PCA


def _parallel_compute_distance(X, cluster):
    n_samples = X.shape[0]
    dis_mat = np.zeros((n_samples, 1))
    for i in range(n_samples):
        dis_mat[i] += np.sqrt(np.sum((X[i] - cluster) ** 2, axis=0))
    return dis_mat


def create_dataset(dataset_dir, text_file="texts.txt", max_len=512):
    data_file = os.path.join(dataset_dir, "data.pkl")
    bow_file = os.path.join(dataset_dir, "bows.mm")
    if os.path.exists(data_file) and os.path.exists(bow_file):
        print_log("Loading encoded texts from {}".format(data_file))
        # data = torch.load(loader_file)
        with open(data_file, 'rb') as f:
            data = pickle.load(f)
        bows = MmCorpus(bow_file)
        print_log("Loaeded.")
    else:
        print_log(f"Reading texts from {os.path.join(dataset_dir, text_file)}")
        tokenizer = BertTokenizer.from_pretrained('bert-base-uncased', do_lower_case=True)
        vocab = tokenizer.get_vocab()
        inv_vocab = {k:v for v, k in vocab.items()}
        corpus = open(os.path.join(dataset_dir, text_file), encoding="utf-8")
        docs = []
        for doc in corpus.readlines():
            content = doc.strip()
            docs.append(content)

        print_log(f"Converting texts into tensors.")
        encoded_dict = tokenizer.batch_encode_plus(docs, add_special_tokens=True, max_length=max_len, padding='max_length',
                                                        return_attention_mask=True, truncation=True, return_tensors='pt')
        input_ids = encoded_dict['input_ids']
        attention_masks = encoded_dict['attention_mask']

        print_log("Generate valid positions...")
        stop_words = set(stopwords.words('english'))
        filter_ids = []
        valid_tag = ["NOUN", "VERB", "ADJ"]
        for i in inv_vocab:
            token = inv_vocab[i]
            if len(token)<2 or token in stop_words or token.startswith('##') \
            or token in string.punctuation or token.startswith('[') \
            or pos_tag([token], tagset='universal')[0][-1] not in valid_tag:
                filter_ids.append(i)
        valid_pos = attention_masks.clone()
        for i in filter_ids:
            valid_pos[input_ids == i] = 0
        valid_ids = [id_ for id_ in inv_vocab if id_ not in filter_ids]

        print_log("Constructing BOW into {}...".format(bow_file))
        bows = []
        for i in range(len(input_ids)):
            bow = {}
            for idx in input_ids[i]:
                idx = idx.item()
                if idx in filter_ids: continue
                if idx in bow:
                    bow[idx] += 1
                else:
                    bow[idx] = 1
            bows.append(list(bow.items()))            

        data = {"input_ids": input_ids, "attention_masks": attention_masks, "valid_pos": valid_pos, "valid_ids": valid_ids}
        with open(data_file, 'wb') as f:
            pickle.dump(data, f)
        MmCorpus.serialize(bow_file, bows)            
    return data, bows


def get_device(id_=0):
    if not torch.cuda.is_available() or id_==-1:
        return torch.device("cpu")
    else:
        return torch.device("cuda:{}".format(id_))


def print_log(s, author="wnj"):
    print("[{}][{}] {}".format(datetime.now().strftime("%m-%d %H:%M:%S"), author, s))


def nvidia_info():
    nvidia_dict = {
        "state": True,
        "nvidia_version": "",
        "nvidia_count": 0,
        "gpus": []
    }
    try:
        pynvml.nvmlInit()
        nvidia_dict["nvidia_version"] = pynvml.nvmlSystemGetDriverVersion()
        nvidia_dict["nvidia_count"] =pynvml. nvmlDeviceGetCount()
        for i in range(nvidia_dict["nvidia_count"]):
            handle = pynvml.nvmlDeviceGetHandleByIndex(i)
            memory_info = pynvml.nvmlDeviceGetMemoryInfo(handle)
            gpu = {
                "gpu_name": pynvml.nvmlDeviceGetName(handle),
                "total": memory_info.total,
                "free": memory_info.free,
                "used": memory_info.used,
                "temperature": f"{pynvml.nvmlDeviceGetTemperature(handle, 0)}℃",
                "powerStatus": pynvml.nvmlDeviceGetPowerState(handle)
            }
            nvidia_dict['gpus'].append(gpu)
    except pynvml.NVMLError as _:
        nvidia_dict["state"] = False
    except Exception as _:
        nvidia_dict["state"] = False
    finally:
        try:
            pynvml.nvmlShutdown()
        except:
            pass
    return nvidia_dict


def wandb_log(title, log_dict, use_wandb=True):
    '''
    Wrap wandb.log function.
    param: title: str like "train/mertic"
    param: log_dict: dict like {"f1": 0.981, "acc": 0.996}
    param: use_wandb: is use_wandb is False, then do nothing
    '''
    if use_wandb == True:
        new_log_dict = {"{}/{}".format(title, k): v for k,v in log_dict.items()}
        wandb.log(new_log_dict)


def check_gpu_mem_usedRate():
    info = nvidia_info()
    # print(info)
    used = info['gpus'][0]['used']
    tot = info['gpus'][0]['total']
    print(f"GPU0 used: {used}, tot: {tot}, 使用率：{used/tot}")


def freeze_parameters(parameters):
    for p in parameters:
        p.requires_grad = False

def unfreeze_parameters(parameters):
    for p in parameters:
        p.requires_grad = True

def calc_cosine_dist_torch(a, b, mode="m2m"):
    '''
    Calculate consine distance between two matrix.
    param: a: torch.Tensor, shape=(size_a, latent_dim)
    param: b: torch.Tensor, shape=(size_b, latent_dim)
    param: mode: "o2o"|"m2m"
    return: res, shape=(size_a, size_b)-"m2m", shape=(size_a)="o2o"
    '''
    a_norm = a / a.norm(dim=1)[:, None]
    b_norm = b / b.norm(dim=1)[:, None]
    if mode == "m2m":
        res = torch.mm(a_norm, b_norm.transpose(0,1))    
    elif mode == "o2o":
        if a_norm.shape != b_norm.shape:
            raise ValueError("a.shape and b.shape should be identical under 'o2o' mode.")
        res = torch.sum(a_norm * b_norm, dim=1)
    return res


def calc_euclidean_dist_torch(a, b, mode="m2m"):
    '''
    Calculate euclidean distance between two matrix.
    param: a: torch.Tensor, shape=(size_a, latent_dim)
    param: b: torch.Tensor, shape=(size_b, latent_dim)
    param: mode: "o2o"|"m2m"
    return: res, shape=(size_a, size_b)-"m2m", shape=(size_a)-"o2o"
    '''    
    if mode == "m2m":
        return torch.cdist(a, b, p=2)
    elif mode == "o2o":
        if a.shape != b.shape:
            raise ValueError("a.shape and b.shape should be identical under 'o2o' mode.")
        return torch.sum((a-b).pow(2), dim=1).sqrt()


def get_internal_npmi(topic_topk_word_ids, bow_mat):
    '''
    param: topic_topk_word_ids: np.Array, sorted topk word index list for each topic, shape=(n_topics, topk)
    param: bow_mat: np.Array, bag-of-word matrix, shape=(n_docs, vocab_size)
    '''
    n_topics = len(topic_topk_word_ids)
    n_docs = len(bow_mat)
    topk = len(topic_topk_word_ids[0])

    npmi = 0.0
    for i in range(n_topics):
        word_array = topic_topk_word_ids[i]
        sum_score = 0.0
        for n in range(topk):
            flag_n = bow_mat[:, word_array[n]] > 0
            p_n = np.sum(flag_n) / n_docs
            for l in range(n + 1, topk):
                flag_l = bow_mat[:, word_array[l]] > 0
                p_l = np.sum(flag_l)
                p_nl = np.sum(flag_n * flag_l)
                #if p_n * p_l * p_nl > 0:
                if p_nl == n_docs:
                    sum_score += 1
                elif p_n > 0 and p_l>0 and p_nl>0:
                    p_l = p_l / n_docs
                    p_nl = p_nl / n_docs
                    sum_score += np.log(p_nl / (p_l * p_n)) / -np.log(p_nl)
        npmi += sum_score * (2 / (topk * topk - topk))
    npmi = npmi / n_topics 
    return npmi


# def get_diversity(topic_topk_word_ids):
#     n_topic, vocab_size = topic_topk_word_ids.shape
#     score = 0
#     for N in N_list:
#         TU = 0.0
#         topic_list = []
#         for topic_idx in range(n_topic):
#             top_word_idx = np.argpartition(beta[topic_idx, :], -N)[-N:]
#             topic_list.append(top_word_idx)
#         TU= 0
#         cnt =[0 for i in range(vocab_size)]
#         for topic in topic_list:
#             for word in topic:
#                 cnt[word]+=1
#         for topic in topic_list:
#             TU_t = 0
#             for word in topic:
#                 TU_t += 1/cnt[word]
#             TU_t /= N
#             TU += TU_t
#         TU /= n_topic
#         score += TU

#     score /= len(N_list)    
#     return score    
    


def get_coherence(beta, doc_mat, N_list=[5,10,15]):
    '''
    Calculate internal NPMI score, NPMI = \sum_j\sum_i log[P(wj, wi)/(P(wj)P(wi))] / -logP(wj, wi)
    beta: word distributions over topics, shape=(n_topics, vocab_size)
    doc_mat: bag-of-word matrix, shape=(n_docs, vocab_size)
    N_list: topk list, default=[5,10,15]
    '''
    topic_size = len(beta)
    doc_size = len(doc_mat)

    average_coherence = 0.0
    for N in N_list:
        # find top words'index of each topic
        topic_list = []
        for topic_idx in range(topic_size):
            top_word_idx = np.argpartition(beta[topic_idx, :], -N)[-N:]
            topic_list.append(top_word_idx)
        # compute coherence of each topic
        sum_coherence_score = 0.0
        for i in range(topic_size):
            word_array = topic_list[i]
            sum_score = 0.0
            for n in range(N):
                flag_n = doc_mat[:, word_array[n]] > 0
                p_n = np.sum(flag_n) / doc_size
                for l in range(n + 1, N):
                    flag_l = doc_mat[:, word_array[l]] > 0
                    p_l = np.sum(flag_l)
                    p_nl = np.sum(flag_n * flag_l)
                    #if p_n * p_l * p_nl > 0:
                    if p_nl == doc_size:
                        sum_score += 1
                    elif p_n > 0 and p_l>0 and p_nl>0:
                        p_l = p_l / doc_size
                        p_nl = p_nl / doc_size
                        sum_score += np.log(p_nl / (p_l * p_n)) / -np.log(p_nl)
            sum_coherence_score += sum_score * (2 / (N * N - N))
        sum_coherence_score = sum_coherence_score / topic_size
        average_coherence += sum_coherence_score
    average_coherence /= len(N_list)
    return average_coherence


def assign_weight_from_freq(vocab_freq, device):
    # return torch.ones_like(vocab_freq).to(device)
    q = torch.tensor([0.25, 0.5, 0.75]).to(device)
    ql = torch.quantile(vocab_freq, q) # shape=(3,)
    vocab_weight = torch.zeros_like(vocab_freq).to(device)
    vocab_weight[torch.where(vocab_freq<=ql[0])] = 0
    vocab_weight[torch.where(vocab_freq>ql[0])] = 0.1
    vocab_weight[torch.where(vocab_freq>ql[1])] = 0.75
    vocab_weight[torch.where(vocab_freq>ql[2])] = 1.0
    return vocab_weight


def tsne_vis(features, labels, centers=None, fig_save_path=None):
    print_log("Drawing T-SNE visualization...")
    tsne = manifold.TSNE(n_components=2, init='pca', random_state=21) # tsne模型
    if centers is not None:
        features = np.concatenate((features, centers), axis=0)
    X_tsne = tsne.fit_transform(features)
    

    x_min, x_max = X_tsne.min(0), X_tsne.max(0)
    X_norm = (X_tsne - x_min) / (x_max - x_min)  # 归一化
    plt.figure(figsize=(8, 8))

    plt.scatter(X_norm[:,0], X_norm[:,1], c=labels, cmap="Paired")
    if centers is not None:
        plt.scatter(X_norm[-len(centers):,0], X_norm[-len(centers):,1], c='k')

    plt.xticks([])
    plt.yticks([])
    if fig_save_path:
        plt.savefig(fig_save_path)
        print_log("T-SNE result saved to: {}".format(fig_save_path))
    else:
        plt.show()



class GMMUtils:
    @classmethod
    def _estimate_log_gaussian_prob(cls, X, means, precisions_chol):
        n_samples, n_features = X.shape
        n_components, _ = means.shape
        log_det = torch.sum(torch.log(precisions_chol), axis=1)
        # print("log_det.shape", log_det.shape, log_det[0])
        precisions = precisions_chol ** 2
        # print("precisions.shape", precisions.shape, precisions[0])
        log_prob = (torch.sum((means**2 * precisions), 1) -
                    2.*torch.mm(X, (means * precisions).T) +
                    torch.mm(X**2, precisions.T))
        # print("log_prob.shape", log_prob.shape, log_prob[0])
        return -0.5 * (n_features * np.log(2*np.pi) + log_prob) + log_det

    @classmethod
    def _estimate_weighted_log_prob(cls, X, means, precisions_chol, weights):
        return GMMUtils._estimate_log_gaussian_prob(X, means, precisions_chol) + torch.log(weights)

    @classmethod
    def _estimate_log_prob_resp(cls, X, means, precisions_chol, weights):
        weighted_log_prob = GMMUtils._estimate_weighted_log_prob(X, means, precisions_chol, weights)
        log_prob_norm = torch.logsumexp(weighted_log_prob, axis=1)
        log_resp = weighted_log_prob - log_prob_norm[:, np.newaxis]
        return log_prob_norm, log_resp  

    @classmethod
    def predict(cls, X, means, covariances, weights):
        proba_mtx = GMMUtils.predict_proba(X, means, covariances, weights)
        return torch.argmax(proba_mtx, dim=1)

    @classmethod
    def predict_proba(cls, X, means, covariances, weights):
        precision_chol = 1. / torch.sqrt(covariances)
        _, log_resp = GMMUtils._estimate_log_prob_resp(X, means, precision_chol, weights)
        proba_mtx = torch.exp(log_resp)
        return proba_mtx        


def get_softmax_guassian_multivirate_expectation(mu, logvar, device, eps=0.607):
    '''
    获取经过softmax变换的多元高斯分布概率期望
    param: mu: 多元高斯分布期望; size=(batch_size, latent_dim); type=Torch.tensor
    param: logvar: 多元高斯分布对数方差; size=(batch_size, latent_dim); type=Torch.tensor
    param: eps: 扰动项
    return: expectation; size=(batch_szie, latent_dim); type=Torch.tensor
    '''
    batch_size, latent_dim = mu.shape[0], mu.shape[1]
    transform_mtx_mu = None
    for i in range(latent_dim):
        ones = torch.ones(1, latent_dim-1).to(device)
        diag = -torch.eye(latent_dim-1).to(device)
        tmp_mtx = torch.cat((diag[:i], ones, diag[i:]), 0)
        if transform_mtx_mu == None:
            transform_mtx_mu = tmp_mtx
        else:
            transform_mtx_mu = torch.cat((transform_mtx_mu, tmp_mtx), 1)
    
    transform_mtx_var = None
    for i in range(latent_dim):
        ones = torch.ones(1, latent_dim-1).to(device)
        diag = torch.eye(latent_dim-1).to(device)
        tmp_mtx = torch.cat((diag[:i], ones, diag[i:]), 0)
        if transform_mtx_var == None:
            transform_mtx_var = tmp_mtx
        else:
            transform_mtx_var = torch.cat((transform_mtx_var, tmp_mtx), 1)

    # transform size=(latent_dim, latent_dim*(latent_dim-1))
    sum_mu = torch.matmul(transform_mtx_mu.T, mu.T)
    # print("sum_mu:", torch.sum(torch.where(sum_mu>0, torch.tensor(1.0).to(device), torch.tensor(0.0).to(device))))
    sum_var = torch.matmul(transform_mtx_var.T, torch.exp(logvar).T)
    re_sigmoid = 1 + torch.exp(- sum_mu / torch.sqrt(1 + eps*eps * sum_var)) # 1/E
    re_sigmoid = torch.sum(re_sigmoid.view(latent_dim, latent_dim-1, batch_size), dim=1).view(latent_dim, batch_size).T
    # print("re_sigmoid:", re_sigmoid)
    
    deno = 2 - latent_dim + re_sigmoid
    deno = torch.where(deno<1e-5, torch.tensor(1.0).to(device), deno)
    expectation = 1.0 / (2 - latent_dim + re_sigmoid)

    return expectation




if __name__ == "__main__":
    check_gpu_mem_usedRate()
import os
import re
import wandb
import hashlib
import argparse
import pickle
import numpy as np
from tqdm import tqdm
from datetime import datetime

import torch
import torch.nn.functional as F
from torch.utils.data import DataLoader, Dataset
from sklearn.mixture import GaussianMixture
from transformers import BertTokenizer

import utils
from utils import GMMUtils
from HeClusTopic import HeClusTopic, AutoEncoderNTM, NVDM_GSM


class MyDataset(Dataset):
    def __init__(self, input_ids, attention_mask, valid_pos, valid_vocab_ids, bows, labels=None) -> None:
        super().__init__()
        tokenizer = BertTokenizer.from_pretrained('bert-base-uncased', do_lower_case=True)
        self.vocab = tokenizer.get_vocab()        
        self.inv_vocab = {v:k for k, v in self.vocab.items()}
        raw_vocab_size = len(self.vocab)

        self.input_ids = input_ids
        self.attention_mask = attention_mask
        self.valid_pos = valid_pos
        self.valid_vocab_ids = valid_vocab_ids # list
        self.vocab_id_2_valid_id = (torch.ones(raw_vocab_size) * -1).to(torch.int64)
        self.vocab_id_2_valid_id[valid_vocab_ids] = torch.arange(len(valid_vocab_ids))
        self.bows = bows
        self.vocab_size = len(valid_vocab_ids)
        if labels:
            self.labels = torch.tensor(labels).to(torch.int)
        else:
            self.labels = torch.ones(len(self.input_ids)) * -1
        self.bow_mtx = np.zeros((len(self.bows), self.vocab_size), dtype=float)
        for doc_idx, doc in enumerate(self.bows):
            for word_idx, count in doc:
                self.bow_mtx[doc_idx, valid_vocab_ids.index(word_idx)] += count           

    def __getitem__(self, idx):
        src_bow = self.bows[idx]
        one_hot_bow = torch.zeros(self.vocab_size)
        item = list(zip(*src_bow))
        one_hot_bow[self.vocab_id_2_valid_id[list(item[0])].to(torch.long)] = torch.tensor(list(item[1])).float()
        # one_hot_bow[self.filter_ids] = 0        
        return self.input_ids[idx], self.attention_mask[idx], self.valid_pos[idx], one_hot_bow, self.labels[idx]
    
    def __len__(self):
        return len(self.input_ids)

    def get_word_by_vocab_id(self, query_vocab_ids):
        return [self.inv_vocab[int(idx)] for idx in query_vocab_ids]

    def get_word_by_valid_id(self, query_valid_ids):
        vocab_ids = [self.valid_vocab_ids[int(idx)] for idx in query_valid_ids]
        return self.get_word_by_vocab_id(vocab_ids)




class HeClusTopicRunner:
    def __init__(self, config) -> None:
        self.device = utils.get_device(config.device)
        config.device = self.device
        utils.print_log("Use device: {}".format(self.device))
        self.config = config
        self.model = None
        self.data_dir = "datasets/{}".format(self.config.dataset)
        self._load_dataset()

    def _load_dataset(self):
        self.data, bows = utils.create_dataset(self.data_dir)
        labels = open(os.path.join(self.data_dir, "labels.txt")).read().splitlines()
        self.label_set = list(set(labels))
        labels = list(map(lambda s:self.label_set.index(s), labels))
        self.dataset = MyDataset(
            self.data["input_ids"], self.data["attention_masks"], self.data["valid_pos"], self.data["valid_ids"],
            bows, labels)
        self.config.vocab_size = self.dataset.vocab_size # valid vocab size

    def _init_model(self, model_path=None):
        self.model = HeClusTopic(self.config)
        if model_path is not None:
            self.model.load_state_dict(torch.load(self.model, map_location=self.device))

    def pretrain_doc_encoder(self):
        '''
        Use classification task pretrain document encoder.
        '''
        utils.print_log("Pretraining document encoder...")
        doc_encoder_path = os.path.join(self.data_dir, "doc_enc.pt")
        if os.path.exists(doc_encoder_path):
            self.model.doc_encoder.load_state_dict(torch.load(doc_encoder_path, map_location=self.device))
            utils.print_log("Load encoder from existed path: {}".format(doc_encoder_path))
        else:
            dataloader = DataLoader(self.dataset, batch_size=self.config.batch_size, shuffle=True)
            encoder = self.model.doc_encoder
            optimizer = torch.optim.Adam(filter(lambda p: p.requires_grad, encoder.parameters()), lr=self.config.lr)
            for epoch in range(self.config.n_pre_epochs):
                for i, batch in enumerate(dataloader):
                    optimizer.zero_grad()
                    input_ids = batch[0].to(self.device)
                    attention_mask = batch[1].to(self.device)
                    valid_pos = batch[2].to(self.device)       
                    labels = batch[4].to(self.device).to(torch.long)
                    preds = encoder.get_cls_output(input_ids, attention_mask, valid_pos)
                    loss = F.cross_entropy(preds, labels)
                    loss.backward()
                    optimizer.step()
                    acc = torch.sum(torch.argmax(preds, dim=1) == labels) / len(labels)
                    utils.print_log("[Epoch-{}|Batch-{}] loss:{:.4f} | acc={:.4f}".format(epoch, i, loss.item(), acc))
            torch.save(encoder.state_dict(), doc_encoder_path)
            utils.print_log("Save encoder to: {}".format(doc_encoder_path))
    
    def pretrain_vae(self, resume_path=None):
        utils.print_log("Pretraining VAE...")
        vae_path = os.path.join(self.data_dir, "vae.pt")
        if os.path.exists(vae_path):
            self.model.vae.load_state_dict(torch.load(vae_path, map_location=self.device))
            utils.print_log("Load vae from existed path: {}".format(vae_path))
        else:
            dataloader = DataLoader(self.dataset, batch_size=self.config.batch_size, shuffle=True)
            vae = self.model.vae
            if resume_path is not None:
                resume_path = os.path.join(self.data_dir, resume_path)
                vae.load_state_dict(torch.load(resume_path, map_location=self.device))
                utils.print_log("Resume pretraining vae from: {}".format(resume_path))
            optimizer = torch.optim.Adam(filter(lambda p: p.requires_grad, vae.parameters()), lr=self.config.lr)
            for epoch in range(self.config.n_vae_pre_epochs):
                loss_dict = {
                    "rec_loss": [],
                    "kld_loss": [],
                    "loss": []
                }
                for i, batch in enumerate(dataloader):
                    optimizer.zero_grad()
                    bows = batch[3].to(self.device)
                    preds, mu, logvar, theta = vae(bows)
                    batch_loss_dict = vae.loss(bows, preds, mu, logvar)
                    batch_loss_dict["loss"].backward()
                    optimizer.step()
                    loss_dict["rec_loss"].append(batch_loss_dict["rec_loss"].item())
                    loss_dict["kld_loss"].append(batch_loss_dict["kld_loss"].item())
                    loss_dict["loss"].append(batch_loss_dict["loss"].item())
                for k, v in loss_dict.items():
                    loss_dict[k] = np.mean(v)
                utils.print_log("[Epoch-{}] {}".format(
                    epoch, " | ".join(["{}:{:.4f}".format(k,v) for k,v in loss_dict.items()])))
                eval_dict = self.eval_and_show_topics(print_=False, save=False)
                utils.print_log("[Epoch-{}] {}".format(
                    epoch, " | ".join(["{}:{:.4f}".format(k,v) for k,v in eval_dict.items()])))
                utils.wandb_log("pretrain-vae/loss", loss_dict, self.config.wandb)
                utils.wandb_log("pretrain-vae/metric", eval_dict, self.config.wandb)
                if (epoch+1) % 20 == 0:
                    self.eval_and_show_topics(print_=True, save=True, suffix="pre_{}_e{}".format(self.config.run_id, epoch+1))

            torch.save(vae.state_dict(), vae_path)
            utils.print_log("Save vae to: {}".format(vae_path))        


    def get_doc_embs(self, detach=False, return_labels=False):
        utils.print_log("Generating documents embeddings...")
        dataloader = DataLoader(self.dataset, batch_size=self.config.batch_size, shuffle=False)
        doc_embs = []
        labels = []
        with torch.no_grad():
            for i, batch in enumerate(tqdm(dataloader)):
                input_ids = batch[0].to(self.device)
                attention_mask = batch[1].to(self.device)
                valid_pos = batch[2].to(self.device)                
                batch_doc_embs = self.model.get_doc_embs(input_ids, attention_mask, valid_pos)
                doc_embs.append(batch_doc_embs)
                labels.append(batch[4])
                # if i==20: break
        doc_embs = torch.cat(doc_embs, dim=0).view(-1, self.config.latent_dim)
        labels = torch.cat(labels, dim=0).view(-1)
        if detach == True:
            doc_embs = doc_embs.detach().cpu().numpy()
            labels = labels.detach().cpu().numpy()
        if return_labels:
            return doc_embs, labels
        return doc_embs


    def init_gmm(self):
        utils.print_log("Fitting GMM...")
        gmm = GaussianMixture(
            n_components=self.config.n_clusters,
            random_state=21, 
            covariance_type="diag", # diag is important
            reg_covar=1e-5
        )

        init_doc_embs_path = os.path.join(self.data_dir, "init_doc_embs.pt")
        if os.path.exists(init_doc_embs_path):
            doc_embs, labels = torch.load(init_doc_embs_path, map_location=self.device)
            utils.print_log("Load init document embeddings from existed path: {}".format(init_doc_embs_path))
        else:
            doc_embs, labels = self.get_doc_embs(detach=True, return_labels=True)
            torch.save((doc_embs, labels), init_doc_embs_path)
            utils.print_log("Save init document embeddings to: {}".format(init_doc_embs_path))

        gmm.fit(doc_embs)
        utils.print_log("GMM fitted. Copying pretrained GMM parameters to HeClusTopic...")
        self.model.init_gmm(gmm.means_, np.log(gmm.covariances_), gmm.weights_)
        # self.visualize_gmm(doc_embs, labels, use_original_labels=True)


    def train(self):
        if self.model is None:
            self.model = HeClusTopic(self.config)
        self.model.to(self.device)
        self.pretrain_doc_encoder()
        self.pretrain_vae("vae_e400.pt")
        self.init_gmm()
        dataloader = DataLoader(self.dataset, batch_size=self.config.batch_size, shuffle=True)
        optimizer = torch.optim.Adam(filter(lambda p: p.requires_grad, self.model.parameters()), lr=self.config.lr)
        print([n for n,p in self.model.named_parameters() if p.requires_grad==True])
        for epoch in range(self.config.n_epochs):
            loss_dict = {
                "rec_loss": [],
                "kld_loss": [],
                "theta_kld_loss": [],
                "loss": []
            }
            for i, batch in enumerate(tqdm(dataloader)):
                optimizer.zero_grad()
                input_ids = batch[0].to(self.device)
                attention_mask = batch[1].to(self.device)
                valid_pos = batch[2].to(self.device)       
                bows = batch[3].to(self.device)                
                doc_embs, logits, mu, logvar, theta = self.model(input_ids, attention_mask, valid_pos, bows)
                batch_loss_dict = self.model.loss(doc_embs, bows, logits, mu, logvar, theta)
                batch_loss_dict["loss"].backward()
                optimizer.step()
                loss_dict["rec_loss"].append(batch_loss_dict["rec_loss"].item())
                loss_dict["kld_loss"].append(batch_loss_dict["kld_loss"].item())
                loss_dict["theta_kld_loss"].append(batch_loss_dict["theta_kld_loss"].item())
                loss_dict["loss"].append(batch_loss_dict["loss"].item())
            for k, v in loss_dict.items():
                loss_dict[k] = np.mean(v)
            utils.print_log("[Epoch-{}] {}".format(
                epoch, " | ".join(["{}:{:.4f}".format(k,v) for k,v in loss_dict.items()])))
            eval_dict = self.eval_and_show_topics(print_=False, save=False)
            utils.print_log("[Epoch-{}] {}".format(
                epoch, " | ".join(["{}:{:.4f}".format(k,v) for k,v in eval_dict.items()])))
            utils.wandb_log("loss", loss_dict, self.config.wandb)
            utils.wandb_log("metric", eval_dict, self.config.wandb)
            if (epoch+1) % 10 == 0:
                self.eval_and_show_topics(print_=True, save=True, suffix="{}_e{}".format(self.config.run_id, epoch+1))
            if (epoch+1) % 10 == 0:
                fig_save_path = os.path.join("results/{}/tsne_{}_e{}.png".format(
                    self.config.dataset, self.config.run_id, epoch+1
                ))
                if not os.path.exists(os.path.dirname(fig_save_path)):
                    os.makedirs(os.path.dirname(fig_save_path))
                self.visualize_gmm(fig_save_path=fig_save_path)
                


    def eval_and_show_topics(self, print_=True, save=True, visual=False, vae_model=None, suffix=""):
        
        if vae_model is None:
            vae_model = self.model.vae
        topic_word_probas = vae_model.decode(torch.eye(self.config.n_topics).to(self.device))
        _, topic_topk_valid_ids = torch.topk(topic_word_probas, k=self.config.k, dim=1)

        npmi = utils.get_internal_npmi(topic_topk_valid_ids, self.dataset.bow_mtx)

        if print_ == True:
            for i in range(len(topic_topk_valid_ids)):
                print("topic-{}: {}".format(
                    i, ",".join(self.dataset.get_word_by_valid_id(topic_topk_valid_ids[i]))))
        if save == True:
            if len(suffix) > 0:
                suffix = "_{}".format(suffix)
            save_path = os.path.join("results/{}/topics{}.txt".format(self.config.dataset, suffix))
            with open(save_path, 'w') as f:
                for i in range(len(topic_topk_valid_ids)):
                    f.write("topic-{}: {}\n".format(
                        i, ",".join(self.dataset.get_word_by_valid_id(topic_topk_valid_ids[i]))))                
        
        eval_dict = {
            "npmi": npmi
        }
        return eval_dict



    def visualize_gmm(self, doc_embs=None, original_labels=None, use_original_labels=True, fig_save_path=None):
        if doc_embs is None or original_labels is None:
            doc_embs, original_labels = self.get_doc_embs(detach=False, return_labels=True)
        if type(doc_embs) != torch.Tensor:
            doc_embs = torch.tensor(doc_embs)
            doc_embs = doc_embs.to(self.device)
        if type(original_labels) == torch.Tensor:
            original_labels = original_labels.detach().cpu().numpy()
        if use_original_labels == False:
            doc_cluster_ids = GMMUtils.predict(
                doc_embs, 
                self.model.gmm_mu, 
                torch.exp(self.model.gmm_logvar), 
                self.model.gmm_weights)
            labels = doc_cluster_ids.detach().cpu().numpy()
        else:
            labels = original_labels
        utils.tsne_vis(doc_embs.detach().cpu().numpy(), 
                    labels=labels,
                    fig_save_path=fig_save_path)



def test_AutoEncoderNTM(config):
        device = utils.get_device(config.device)
        config.device = device
        utils.print_log("Use device: {}".format(device))
        data_dir = "datasets/{}".format(config.dataset)
        data, bows = utils.create_dataset(data_dir)
        labels = open(os.path.join(data_dir, "labels.txt")).read().splitlines()
        label_set = list(set(labels))
        labels = list(map(lambda s:label_set.index(s), labels))
        dataset = MyDataset(
            data["input_ids"], data["attention_masks"], data["valid_pos"], data["valid_ids"],
            bows, labels)
        config.vocab_size = dataset.vocab_size # valid vocab size

        # model = AutoEncoderNTM(config)
        model = NVDM_GSM(config)
        model.to(device)
        dataloader = DataLoader(dataset, batch_size=config.batch_size, shuffle=True)
        optimizer = torch.optim.Adam(filter(lambda p: p.requires_grad, model.parameters()), lr=config.lr)
        for epoch in range(config.n_epochs):
            epoch_loss = 0
            epoch_rec_loss = 0
            epoch_kld_loss = 0
            for i, batch in enumerate(dataloader):   
                optimizer.zero_grad()
                bows = batch[3].to(device)     
                preds = model(bows)
                preds, mu, logvar, theta = model(bows)
                # loss = model.loss(bows, preds)
                rec_loss, kld_loss = model.loss(bows, preds, mu, logvar)
                loss = rec_loss + kld_loss
                loss.backward()
                optimizer.step()
                # utils.print_log("[Epoch-{}|Batch-{}] loss:{:.4f}".format(epoch, i, loss.item()))
                epoch_loss += loss.item()
                epoch_rec_loss += rec_loss.item()
                epoch_kld_loss += kld_loss.item()
            epoch_loss = epoch_loss / i
            epoch_rec_loss = epoch_rec_loss / i
            epoch_kld_loss = epoch_kld_loss / i
            topic_word_probas = model.decode(torch.eye(config.n_topics).to(device))
            _, topic_topk_valid_ids = torch.topk(topic_word_probas, k=config.k, dim=1)
            npmi = utils.get_internal_npmi(topic_topk_valid_ids, dataset.bow_mtx)
            # utils.print_log("[Epoch-{}] loss:{:.4f} | NPMI:{:.4f}".format(epoch, epoch_loss, npmi))
            utils.print_log("[Epoch-{}] loss:{:.4f} | rec_loss:{:.4f} | kld_loss:{:.4f} | NPMI:{:.4f}".format(
                epoch, epoch_loss, epoch_rec_loss, epoch_kld_loss, npmi))
            if (epoch+1)%10 == 0:
                for i in range(len(topic_topk_valid_ids)):
                    print("topic-{}: {}".format(
                        i, ",".join(dataset.get_word_by_valid_id(topic_topk_valid_ids[i]))))            




if __name__ == "__main__":
    parser = argparse.ArgumentParser()

    parser.add_argument('--seed', type=int, default=21)
    parser.add_argument('--device', type=int, default=0)
    parser.add_argument('--dataset', default='20news')
    parser.add_argument('--batch_size', type=int, default=64)
    parser.add_argument('--lr', type=float, default=1e-4)
    parser.add_argument('--n_clusters', default=20, type=int, help='number of topics')
    parser.add_argument('--n_topics', default=100, type=int, help='number of topics')
    parser.add_argument('--k', default=10, type=int, help='number of top words to display per topic')
    parser.add_argument('--bert_dim', default=768, type=int, help='embedding dimention of pretrained language model')
    parser.add_argument('--latent_dim', default=128, type=int, help='latent embedding dimention')
    parser.add_argument('--hidden_dim', default=256, type=int, help='latent embedding dimention')
    parser.add_argument('--n_epochs', default=20, type=int, help='number of epochs for clustering')
    parser.add_argument('--n_pre_epochs', default=20, type=int, help='number of epochs for pretraining autoencoder')
    parser.add_argument('--n_vae_pre_epochs', default=100, type=int, help='number of epochs for pretraining autoencoder')
    parser.add_argument("--wandb_log_interval", default=50, type=int)
    parser.add_argument("--tsne_interval", default=5, type=int)
    parser.add_argument('--train', action='store_true')
    parser.add_argument('--test', action='store_true')
    parser.add_argument('--finetune_bert', action='store_true')
    parser.add_argument("--wandb", action="store_true", help="whether to log at wandb")

    args = parser.parse_args()

    np.random.seed(args.seed)
    torch.manual_seed(args.seed)
    torch.cuda.manual_seed_all(args.seed)
    torch.backends.cudnn.deterministic = True
    torch.backends.cudnn.benchmark = False

    if args.test:
        test_AutoEncoderNTM(args)

    if args.train:
        hash_seed = datetime.now().strftime("%y-%m-%d %H:%M:%S")
        run_id = hashlib.md5(hash_seed.encode('utf8')).hexdigest()[:6]
        args.run_id = run_id

        print(args)

        if args.wandb:
            wandb.init(
                project="HeClusTopicNew",
                name=run_id,
                config=args
            )

        runner = HeClusTopicRunner(args)        
        runner.train()    
import os
import random
from tqdm import tqdm
import torch
import torch.nn.functional as F
from torch.utils.data import DataLoader
import numpy as np
from transformers import BertTokenizer
from sklearn.cluster import KMeans
from sklearn.manifold import TSNE
import matplotlib.pyplot as plt
import utils
from model_utils import MyDataset
from model import HeClusTopicModel


def cluster_doc_embs(config):
    '''
    王老师：对文档聚类簇和类别标签做可视化，看能不能对上
    '''
    data_dir = "datasets/{}".format(config.dataset)
    config.device = utils.get_device()

    doc_embs_path = os.path.join(data_dir, "doc_embs.pt")
    if os.path.exists(doc_embs_path):
        doc_embs, doc_labels = torch.load(doc_embs_path)
        print(len(set(doc_labels)))
    else:
        model = HeClusTopicModel(config)
        model.to(config.device)
        vocab = BertTokenizer.from_pretrained("bert-base-uncased").get_vocab()
        # get doc embeddings
        data, bows = utils.create_dataset(data_dir)
        labels = open(os.path.join(data_dir, "labels.txt")).read().splitlines()
        label_set = list(set(labels))
        labels = list(map(lambda s:label_set.index(s), labels))
        dataset = MyDataset(data["input_ids"], data["attention_masks"], data["valid_pos"], bows, len(vocab), labels)
        dataloader = DataLoader(dataset, batch_size=config.batch_size)

        doc_embs = []
        doc_labels = []
        with torch.no_grad():
            for i, batch in enumerate(tqdm(dataloader)):
                input_ids = batch[0].to(config.device)
                attention_mask = batch[1].to(config.device)
                valid_pos = batch[2].to(config.device)
                batch_doc_embs = model.get_doc_emb(input_ids, attention_mask, valid_pos)
                doc_embs.append(batch_doc_embs)
                doc_labels.append(batch[4].numpy())
                # if i==10: break
        doc_embs = np.concatenate(doc_embs, axis=0)
        doc_labels = np.concatenate(doc_labels, axis=0)
        torch.save((doc_embs, doc_labels), doc_embs_path)

    kmeans = KMeans(n_clusters=config.n_clusters)
    pred_labels = kmeans.fit_predict(doc_embs)

    # sample_index = random.sample(range(len(pred_labels)), k=2000)
    # doc_embs = doc_embs[sample_index]
    # pred_labels = pred_labels[sample_index]
    # doc_labels = doc_labels[sample_index]

    tsne = TSNE(n_components=2, random_state=21)
    embs = tsne.fit_transform(doc_embs)

    fig, ax = plt.subplots(1,2)
    ax[0].scatter(embs[:,0], embs[:,1], c=doc_labels, cmap="tab20c")
    ax[1].scatter(embs[:,0], embs[:,1], c=pred_labels, cmap="tab20c")
    plt.show()



# if __name__ =="__main__":
#     cluster_doc_embs()
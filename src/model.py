from transformers import BertModel, BertTokenizer
import numpy as np
from joblib import Parallel, delayed
from sklearn.cluster import KMeans
import torch
from torch import nn
from torch.nn.parameter import Parameter
import torch.nn.functional as F

import utils

class KmeansBatch:
    def __init__(self, config):
        self.config = config
        self.n_features = config.latent_dim
        self.n_clusters = config.n_clusters
        self.cluster_centers = np.zeros((self.n_clusters, self.n_features))
        self.count = 100 * np.ones((self.n_clusters))  # serve as learning rate
        self.is_init = False

    def _compute_dist(self, X):
        dis_mat = Parallel(n_jobs=1)(
            delayed(utils._parallel_compute_distance)(X, self.cluster_centers[i])
            for i in range(self.n_clusters))
        dis_mat = np.hstack(dis_mat)

        return dis_mat

    def init_cluster(self, X, sample_weight=None):
        """ Generate initial clusters using sklearn.Kmeans """
        model = KMeans(n_clusters=self.n_clusters)
        # model.fit(X)
        model.fit(X, sample_weight=sample_weight)
        self.cluster_centers = torch.tensor(model.cluster_centers_).to(self.config.device)  # copy clusters
        self.is_init = True

    def update_cluster(self, latent_embs, cluster_idx, weights):
        """ Update clusters in Kmeans on a batch of data """
        with torch.no_grad():
            n_samples = latent_embs.shape[0]
            for i in range(n_samples):
                self.count[cluster_idx] += 1
                eta = 1.0 / self.count[cluster_idx] * weights[i]
                updated_cluster = ((1 - eta) * self.cluster_centers[cluster_idx] +
                                eta * latent_embs[i])
                self.cluster_centers[cluster_idx] = updated_cluster


    def assign_cluster(self, X, mode="hard"):
        """ Assign samples in `X` to clusters """
        cluster_centers = F.normalize(self.cluster_centers, dim=-1)
        sim = torch.matmul(X, cluster_centers.transpose(0,1))
        p = F.softmax(sim, dim=-1)
        if mode == "hard":
            return torch.argmax(p, axis=1)
        elif mode == "soft":
            return p




class AutoEncoder(nn.Module):
    ''' Use AutoEncoder to initialize HeClusTopicModel's encoder. '''
    def __init__(self, config) -> None:
        super().__init__()
        self.encoder = nn.Sequential(
            nn.Linear(config.bert_dim, 512), 
            nn.ReLU(),
            nn.Linear(512, config.latent_dim)
        )
        self.decoder = nn.Sequential(
            nn.Linear(config.latent_dim, 512), 
            nn.ReLU(), 
            nn.Linear(512, config.bert_dim)
        )
    
    def get_latent_emb(self, x):
        z = self.encoder(x)
        z = F.normalize(z, dim=-1)
        return z        

    def forward(self, x):
        z = self.encoder(x)
        z = F.normalize(z, dim=-1)
        x_rec = self.decoder(z)
        return x_rec, z    



class HeClusTopicModel(nn.Module):
    def __init__(self, config) -> None:
        super().__init__()
        self.config = config
        self.device = config.device
        self.bert = BertModel.from_pretrained("bert-base-uncased", output_hidden_states=True)
        self.vocab = BertTokenizer.from_pretrained("bert-base-uncased").get_vocab()
        self.inv_vocab = {v:k for k,v in self.vocab.items()}
        # self.kmeans = KmeansBatch(config)
        self.topic_emb = nn.Parameter(torch.Tensor(config.n_clusters, config.latent_dim))
        self.kappa = config.kappa
        self.ae = AutoEncoder(self.config)    
        # set bert output layers
        n_layers = 4
        self.layers = list(range(-n_layers, 0))
        self.vocab_weights = None # initialized in _pre_clustering()

        # torch.nn.init.xavier_normal_(self.topic_emb.data)

    
    def assign_cluster(self, z):
        self.topic_emb.data = F.normalize(self.topic_emb.data, dim=1)
        sim = torch.mm(z, self.topic_emb.t()) * self.kappa
        p = F.softmax(sim, dim=1)
        return p


    def target_distribution(self, p):
        targets = p**2 / p.sum(dim=0)
        targets = (targets.t() / targets.sum(dim=1)).t()
        return targets        

    
    def bert_encode(
            self,
            input_ids:torch.Tensor, 
            attention_mask:torch.Tensor, 
        ):
        # TODO: BERT embedding calculation reconsider
        # bert_outputs = self.bert(input_ids, attention_mask=attention_mask)
        # specified_hidden_states = [bert_outputs.hidden_states[i] for i in self.layers]
        # specified_embeddings = torch.stack(specified_hidden_states, dim=0)
        # token_embeddings = torch.squeeze(torch.mean(specified_embeddings, dim=0))
        bert_outputs = self.bert(input_ids, attention_mask=attention_mask)
        token_embeddings = bert_outputs[0]
        return token_embeddings


    def forward(
            self, 
            input_ids:torch.Tensor, 
            attention_mask:torch.Tensor, 
            valid_pos:torch.Tensor,
            bows: torch.Tensor,
        ):
        
        '''
        Get valid word ids, co-occur matrix, latent embeddings and cluster assignments.
        return: valid_ids: valid input ids, shape=(torch.sum(valid_pos),)
        return: latent_embs: valid latent embeddings, shape=(torch.sum(valid_pos), latent_dim)
        return: bert_embs: valid bert embeddings, shape=(torch.sum(valid_pos), bert_dim)
        return: bert_embs_rec: valid bert embeddings, shape=(torch.sum(valid_pos), latent_dim)
        return: p_tokens: valid token's topic distribution matrix, shape=(torch.sum(valid_pos), n_clusters)
        return: valid_bows: valid bag-of-word one-hot representations
        '''
        # bert + encoder
        batch_size, seq_length = input_ids.shape
        latent_dim, n_clusters, k_co = self.config.latent_dim, self.config.n_clusters, self.config.k_co
        valid_mask = valid_pos != 0
        filter_mask = valid_pos == 0
        bert_embs = self.bert_encode(input_ids, attention_mask) # batch_size, 512, bert_dim
        bert_embs_rec, latent_embs = self.ae(bert_embs) # shape=(torch.sum(valid_pos), latent_dim)
        
        # cluster assignments
        tokens_p_topic = self.assign_cluster(
            latent_embs.view(-1, latent_dim)).view(batch_size, seq_length, -1) # batch_size, seq_length, n_clusters

        # captain election
        tokens_highest_p_topic, _ = torch.max(tokens_p_topic, dim=-1) # batch_size, seq_length
        tokens_highest_p_topic[filter_mask] = 0
        _, ids = torch.topk(tokens_highest_p_topic*self.vocab_weights[input_ids].view(batch_size,-1), k=k_co, dim=-1) # batch_size, 10
        top_tokens_p_topic = torch.zeros(batch_size, k_co, n_clusters)
        for i, doc_ids in enumerate(ids):
            top_tokens_p_topic[i] = tokens_p_topic[i][doc_ids]
        
        bert_embs = bert_embs[valid_mask]
        bert_embs_rec = bert_embs_rec[valid_mask]
        tokens_p_topic = tokens_p_topic[valid_mask]
        valid_ids = input_ids[valid_mask]

        return valid_ids, bert_embs, bert_embs_rec, tokens_p_topic, top_tokens_p_topic


    def get_latent_emb(self, input_ids, attention_mask, valid_pos=None):
        last_hidden_states = self.bert_encode(input_ids, attention_mask)
        if valid_pos is not None:
            mask = valid_pos != 0
            latent_embs = self.ae.get_latent_emb(last_hidden_states[mask])
        else:
            latent_embs = self.ae.get_latent_emb(last_hidden_states)
        return latent_embs


    def get_doc_emb(self, input_ids, attention_mask, valid_pos):
        batch_size = len(input_ids)
        latent_dim = self.config.latent_dim
        valid_mask = valid_pos != 0
        bert_embs = self.bert_encode(input_ids, attention_mask) # batch_size, 512, bert_dim
        _, latent_embs = self.ae(bert_embs) # batch_size, 512, bert_dim
        # latent_embs = bert_embs
        doc_embs = torch.zeros(batch_size, latent_dim)   
        for i in range(batch_size):
            doc_embs[i] = torch.mean(latent_embs[i][valid_mask[i]], dim=0)
        return doc_embs
    

    def get_loss(self, valid_ids, bert_embs, bert_embs_rec, tokens_p_topic, top_tokens_p_topic):
        '''
        param: valid_ids: shape=(batch_valid_size,)
        param: bert_embs: valid bert embeddings, shape=(batch_valid_size, bert_dim)
        param: bert_embs_rec: valid bert embeddings, shape=(batch_valid_size, bert_dim)        
        param: tokens_p_topic: shape=(batch_valid_size, n_clusters)
        param: top_tokens_p_topic: shape=(batch_size, topk, n_clusters)
        '''
        # rec loss
        rec_loss = F.mse_loss(bert_embs_rec, bert_embs)

        # cluster loss
        token_weights = self.vocab_weights[valid_ids]
        tokens_q_topic = self.target_distribution(tokens_p_topic).detach()
        clus_loss = -torch.mean(torch.sum(tokens_p_topic.log() * tokens_q_topic, dim=1) * token_weights)
        # clus_loss = F.kl_div(tokens_p_topic.log(), tokens_q_topic, reduction='batchmean')

        # co-occur loss
        co_loss = torch.tensor(0.).to(self.device)
        batch_size = top_tokens_p_topic.shape[0]
        for i in range(batch_size):
            tokens_p_topic = top_tokens_p_topic[i]
            co_loss += torch.sum(utils.calc_cosine_dist_torch(tokens_p_topic, tokens_p_topic, mode="m2m"))
        co_loss = -co_loss / batch_size / self.config.k_co

        total_loss = rec_loss + clus_loss * self.config.clus_weight + co_loss * self.config.co_weight
    
        loss = {
            "rec_loss": rec_loss,
            "kmeans_loss": clus_loss,
            "co_loss": co_loss,
            "total_loss": total_loss
        } 

        return loss
